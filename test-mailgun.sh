#!/bin/bash

templates=(banned_member disclaim_receiver disclaim_sender inactive_member)
templates+=(incomplate_member invite_member password_change)
templates+=(receiver_expired receiver_referral sample_receiver_disclaimed_referral)
templates+=(sender_accepted sender_cancelled sender_declined)
templates+=(sender_disclaimed_accpeted sender_expired sender_invitation sender)
templates+=(verify_email welcome_member)

# to='George Everitt <geveritt@appliedrelevance.com>'
# to='Justin Banford <justinbanford@gmail.com>'
to='admin@inthority.com'

for template in ${templates[*]}
do
curl -s --user 'api:df5591cf049c55ac5d8a5fbfdafb4c11-060550c6-e9a131d4' \
	 https://api.mailgun.net/v3/mg.inthority.com/messages \
	 -F from='Mailgun Sandbox <postmaster@mg.inthority.com>' \
	 -F to="$to" \
	 -F subject="Test $template" \
	 -F template="$template" \
	 -F h:X-Mailgun-Variables='{"test": "test"}'
done
echo 'Finished.'